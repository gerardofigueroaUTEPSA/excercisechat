package com.chatserver.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.chatserver.model.Role.getAll",
                query = "SELECT r FROM Role r"
        )
})
@ApiModel(value = "Role entity", description = "info a entity role")
public class Role {
    private long id;
    private String name;

    public Role(){ }

    public Role(long id, String name){
        this.id = id;
        this.name = name;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "the id of de role", required = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false)
    @ApiModelProperty(value = "the name of de role", required = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
