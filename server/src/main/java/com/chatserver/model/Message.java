package com.chatserver.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.chatserver.model.Message.getAllByIdCreator",
                query = "SELECT m FROM Message m"
        )
})
@ApiModel(value = "Message Entity", description = "info of a entity message")
public class Message {
    private long id;
    private Credential idCreator;
    private Credential idReceiver;
    private DateTime dateSent;
    private DateTime dateSeen;
    private String content;

    public Message() { }

    public Message(long id, Credential idCreator, Credential idReceiver, DateTime dateSent, DateTime dateSeen, String content){
        this.id = id;
        this.idCreator = idCreator;
        this.idReceiver = idReceiver;
        this.dateSent = dateSent;
        this.dateSeen = dateSeen;
        this.content = content;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "the id of the message")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn(name = "id_creator", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The creator of the message", required = true)
    public Credential getCreator() {
        return idCreator;
    }

    public void setCreator(Credential idCreator) {
        this.idCreator = idCreator;
    }

    @ManyToOne
    @JoinColumn(name = "id_receiver", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The receiver of the message", required = true)
    public Credential getReceiver() {
        return idReceiver;
    }

    public void setReceiver(Credential idReceiver) {
        this.idReceiver = idReceiver;
    }

    @Basic
    @Column(name = "date_sent", nullable = false)
    @ApiModelProperty(value = "The date sent of the message", required = true)
    public DateTime getDateSent() {
        return dateSent;
    }

    public void setDateSent(DateTime dateSent) {
        this.dateSent = dateSent;
    }

    @Basic
    @Column(name = "date_seen", nullable = false)
    @ApiModelProperty(value = "The date seen of the message", required = true)
    public DateTime getDateSeen() {
        return dateSeen;
    }

    public void setDateSeen(DateTime dateSeen) {
        this.dateSeen = dateSeen;
    }

    @Basic
    @Column(name = "content", nullable = false)
    @ApiModelProperty(value = "The content of the message", required = true)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", idCreator=" + idCreator +
                ", idReceiver=" + idReceiver +
                ", dateSent=" + dateSent +
                ", dateSeen=" + dateSeen +
                ", content='" + content + '\'' +
                '}';
    }
}
