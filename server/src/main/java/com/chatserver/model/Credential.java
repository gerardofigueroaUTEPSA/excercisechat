package com.chatserver.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.persistence.*;

@Entity
@NamedQueries({
        @NamedQuery(
                name = "com.chatserver.model.Credential.login",
                query = "SELECT u FROM Credential u WHERE u.username = :username AND u.password = :password AND u.state = true"
        ),
        @NamedQuery(
                name = "com.chatserver.model.Credential.getByUsername",
                query = "SELECT u FROM Credential u WHERE u.username = :username AND u.state = true"
        ),
        @NamedQuery(
                name = "com.chatserver.model.Credential.getCredentialActive",
                query = "SELECT u FROM Credential u WHERE u.state = true"
        )
})
@ApiModel(value = "Credential Entity", description = "info of a entity credential")
public class Credential {
    private long id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private boolean state;
    private Role role;

    public Credential(){ }

    public Credential(long id, String firstName, String lastName, String username,
                      String password, boolean state, Role role){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.state = state;
        this.role = role;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @ApiModelProperty(value = "the id of the credential")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "first_name", nullable = false)
    @ApiModelProperty(value = "The first name of the credential", required = true)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false)
    @ApiModelProperty(value = "The last name of the credential", required = true)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "username", nullable = false)
    @ApiModelProperty(value = "The username of the credential", required = true)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false)
    @ApiModelProperty(value = "The password of the credential", required = true)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "state", nullable = false)
    @ApiModelProperty(value = "The state of the credential", required = true)
    public boolean isState() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName = "id", nullable = false)
    @ApiModelProperty(value = "The role of the credential in application", required = true)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String  toString() {
        return "Credential{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", state=" + state +
                ", role=" + role +
                '}';
    }
}
