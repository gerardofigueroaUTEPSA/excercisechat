package com.chatserver.core;

import com.chatserver.api.response.BasicResponse;
import java.util.ArrayList;
import java.util.List;

public class Operations {
    public static BasicResponse getResponseErrors(Exception e){
        BasicResponse response = new BasicResponse();
        response.setCode(StatusCode.INTERNAL_SERVER_ERROR);
        response.setMessage("If an error occurred our server");

        List<String> errors = new ArrayList<>();
        if(!e.getMessage().isEmpty()){
            errors.add(e.getMessage());
        }
        if(e.getCause().getMessage().isEmpty()){
            errors.add(e.getCause().getMessage());
        }
        if(errors != null){
            response.setData(errors);
        }

        return  response;
    }
}
