package com.chatserver.core;

public class StatusCode {
    public final static int OK = 200;
    public final static int CREATED = 201;
    public final static int NO_CONTENT = 204;
    public final static int NOT_MODIFIED = 304;
    public final static int UNAUTHORIZED = 401;
    public final static int FORBIDDEN = 403;
    public final static int NOT_FOUND = 404;
    public final static int CONFLICT = 409;
    public final static int INTERNAL_SERVER_ERROR = 500;
}
