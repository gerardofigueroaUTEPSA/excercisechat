package com.chatserver.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class ChatServerConfig extends Configuration {

    private String serverName;

    @Valid
    @NotNull
    private DataSourceFactory database = new DataSourceFactory();

    public ChatServerConfig() {}

    @JsonProperty
    public String getServerName() {
        return this.serverName;
    }

    @JsonProperty("database")
    public DataSourceFactory getDataSourceFactory() {
        return this.database;
    }

    @JsonProperty
    public void setDataSourceFactory(DataSourceFactory dataSourceFactory) {
        this.database = dataSourceFactory;
    }
}
