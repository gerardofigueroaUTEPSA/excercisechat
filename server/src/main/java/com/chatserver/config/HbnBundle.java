package com.chatserver.config;

import com.chatserver.model.*;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;

public class HbnBundle  extends HibernateBundle<ChatServerConfig> {
    public HbnBundle() {
        super(Role.class, Credential.class, Message.class);
    }

    @Override
    public PooledDataSourceFactory getDataSourceFactory(ChatServerConfig configuration) {
        return configuration.getDataSourceFactory();
    }
}
