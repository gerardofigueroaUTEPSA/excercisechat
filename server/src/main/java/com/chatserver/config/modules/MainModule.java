package com.chatserver.config.modules;

import com.chatserver.config.ChatServerConfig;
import ru.vyarus.dropwizard.guice.module.support.DropwizardAwareModule;

public class MainModule extends DropwizardAwareModule<ChatServerConfig> {
    @Override
    protected void configure() {

    }
}
