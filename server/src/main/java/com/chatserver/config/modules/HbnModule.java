package com.chatserver.config.modules;

import com.chatserver.dao.credential.CredentialDAO;
import com.chatserver.dao.credential.CredentialRealDAO;
import com.chatserver.dao.message.MessageDAO;
import com.chatserver.dao.message.MessageRealDAO;
import com.chatserver.dao.role.RoleDAO;
import com.chatserver.dao.role.RoleRealDAO;
import com.google.inject.AbstractModule;
import com.chatserver.config.HbnBundle;
import org.hibernate.SessionFactory;

public class HbnModule extends AbstractModule {
    private final HbnBundle hbnBundle;

    public HbnModule(HbnBundle hbnBundle) {
        this.hbnBundle = hbnBundle;
    }

    @Override
    protected void configure() {
        bind(SessionFactory.class).toInstance(hbnBundle.getSessionFactory());
        bind(RoleDAO.class).toInstance(new RoleRealDAO(hbnBundle.getSessionFactory()));
        bind(CredentialDAO.class).toInstance(new CredentialRealDAO(hbnBundle.getSessionFactory()));
        bind(MessageDAO.class).toInstance(new MessageRealDAO(hbnBundle.getSessionFactory()));
    }
}
