package com.chatserver.resource.credential;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import com.chatserver.api.response.BasicResponse;
import com.chatserver.core.StatusCode;
import com.chatserver.model.Credential;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(CredentialResource.SERVICE_PATH)
@Api(value = CredentialResource.SERVICE_PATH, description = "Operations about Credentials")
@Produces(MediaType.APPLICATION_JSON)
public class CredentialResource {
    public final static String SERVICE_PATH = "/user";
    private final CredentialService service;

    @Inject
    public CredentialResource(CredentialService service){
        this.service = service;
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation( value = "Gets the credential by id", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Credential found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Credential not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getById(@ApiParam(value = "Credential ID needed to find the credential", required = true)
                                     @PathParam("id") long id) {
        return service.getById(id);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/active")
    @ApiOperation( value = "Gets the credential active", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Credential found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Credential not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getById() {
        return service.getCredentialActive();
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/create")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a credential", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The credential has been created"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse create(
            @ApiParam(value = "JSON format is received with the role structure.", required = true) Credential credential) {
        return service.create(credential);
    }

    @POST
    @Timed
    @UnitOfWork
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces("application/json")
    @ApiOperation( value = "Allows a user to initiate a session", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Signed in successfully"),
            @ApiResponse(code = StatusCode.FORBIDDEN, message = "You must change your password"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "User does not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal Server Error")})
    public BasicResponse loginUser(@ApiParam(value = "Username needed to login", required = true)
                                        @HeaderParam("username") String username,
                                   @ApiParam(value = "Password needed to login", required = true)
                                        @HeaderParam("password") String password) {
        try {
            return service.login(username, password);
        } catch (Exception e) {
            throw new WebApplicationException(e.getMessage(), Response.Status.NOT_ACCEPTABLE);
        }
    }

    @PUT
    @Timed
    @UnitOfWork
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Modifies the profile of credentials.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The  credential profile was modified"),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "The credential profile was not modified because it is null"),
            @ApiResponse(code = StatusCode.NOT_MODIFIED, message = "The credential profile was not modified because credential administrator not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse modify(
            @ApiParam(value = "JSON format is received with the Credential structure.", required = true) Credential credential) {
        return service.modify(credential);
    }


    @DELETE
    @Timed
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation(value = "Deletes a credential by id.", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "The credential was deleted"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "This credential was not exist"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse delete(@PathParam("id") long id) {
        return service.delete(id);
    }
}
