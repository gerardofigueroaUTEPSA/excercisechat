package com.chatserver.resource.credential;

import com.google.inject.Inject;
import com.chatserver.api.response.BasicResponse;
import com.chatserver.core.Operations;
import com.chatserver.core.StatusCode;
import com.chatserver.dao.credential.CredentialDAO;
import com.chatserver.dao.role.RoleDAO;
import com.chatserver.model.Credential;
import com.chatserver.model.Role;

import java.util.List;

public class CredentialService {
    private final CredentialDAO credentialDAO;
    private final RoleDAO roleDAO;

    @Inject
    public CredentialService(CredentialDAO credentialDAO, RoleDAO roleDAO){
        this.credentialDAO = credentialDAO;
        this.roleDAO = roleDAO;
    }

    public BasicResponse create(Credential credential) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            if(credentialDAO.getByUsername(credential.getUsername().trim()) == null){
                Role role = roleDAO.getById(credential.getRole().getId());
                credential.setRole(role);
                credentialDAO.create(credential);
                basicResponse.setCode(StatusCode.CREATED);
                basicResponse.setMessage("Credential created correctly");
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("User Exist");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getById(long id){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Credential credential = credentialDAO.getById(id);
            if(credential != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(credential);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This credential was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getCredentialActive(){
        BasicResponse basicResponse = new BasicResponse();
        try {
            List<Credential> credentials = credentialDAO.getCredentialActive();
            if(credentials.size() > 0){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(credentials);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("There are no credentials");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse login(String username, String password){
        BasicResponse basicResponse = new BasicResponse();
        Credential credential = null;
        try {
            credential = credentialDAO.login(username, password);
            if(credential != null){
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setUniqueResult(credential);
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("The user does not exist");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse delete(long id){
        BasicResponse basicResponse = new BasicResponse();
        try{
            Credential credential = credentialDAO.getById(id);

            if(credential.isState() == false){
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("the credential not exist");
            }

            if(credential.isState() == true){
                credentialDAO.delete(id);
                basicResponse.setCode(StatusCode.OK);
                basicResponse.setMessage("delete correctly");
            } else {
                basicResponse.setCode(StatusCode.INTERNAL_SERVER_ERROR);
            }

        }
        catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse modify(Credential credential){
        BasicResponse basicResponse = new BasicResponse();
        try{
            Credential credentials = credentialDAO.getById(credential.getId());
            if(credentials != null){
                if(credentials.isState() != false){
                    credentialDAO.modify(credential);
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setMessage("Modified correctly");
                }else {
                    basicResponse.setCode(StatusCode.NOT_MODIFIED);
                    basicResponse.setMessage("It has not been modified because credential administrator not found");
                }
            } else {
                basicResponse.setCode(StatusCode.NOT_MODIFIED);
                basicResponse.setMessage("It has not been modified because it is null");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}

