package com.chatserver.resource.message;

import com.chatserver.api.response.BasicResponse;
import com.chatserver.core.Operations;
import com.chatserver.core.StatusCode;
import com.chatserver.dao.credential.CredentialDAO;
import com.chatserver.dao.message.MessageDAO;
import com.chatserver.model.Credential;
import com.chatserver.model.Message;
import com.google.inject.Inject;
import org.joda.time.DateTime;
import java.util.List;

public class MessageService {
    private final MessageDAO messageDAO;
    private final CredentialDAO credentialDAO;

    @Inject
    public MessageService(MessageDAO messageDAO, CredentialDAO credentialDAO){
        this.messageDAO = messageDAO;
        this.credentialDAO = credentialDAO;
    }

    public BasicResponse create(long idCreator, long idReceiver, String msg) {
        BasicResponse basicResponse = new BasicResponse();
        try {
            Credential creator = credentialDAO.getById(idCreator);
            Credential receiver = credentialDAO.getById(idReceiver);

            if(creator != null && receiver != null){
                DateTime date = new DateTime();
                messageDAO.create(new Message(0, creator, receiver, date, null, msg));
                basicResponse.setCode(StatusCode.CREATED);
                basicResponse.setMessage("Message created correctly");
            } else {
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("User not Exist");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAllByIdUser(long idUser){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Credential credential = credentialDAO.getById(idUser);
            if(credential != null){
                List<Message> messages = messageDAO.getAllByIdUser(idUser);
                if(messages.size() > 0){
                    messageDAO.seenMessage(messages);
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setUniqueResult(messages);
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This message was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }

    public BasicResponse getAllByUserOneAndUserTwo(long userOne, long userTwo){
        BasicResponse basicResponse = new BasicResponse();
        try {
            Credential credentialOne = credentialDAO.getById(userOne);
            Credential credentialTwo = credentialDAO.getById(userTwo);
            if(credentialOne != null && credentialTwo != null){
                List<Message> messages = messageDAO.getAllByUserOneAndUserTwo(userOne, userTwo);
                if(messages.size() > 0){
                    messageDAO.seenMessage(messages);
                    basicResponse.setCode(StatusCode.OK);
                    basicResponse.setUniqueResult(messages);
                }
            }else{
                basicResponse.setCode(StatusCode.NOT_FOUND);
                basicResponse.setMessage("This message was not found");
            }
        } catch (Exception e) {
            basicResponse = Operations.getResponseErrors(e);
        }
        return basicResponse;
    }
}
