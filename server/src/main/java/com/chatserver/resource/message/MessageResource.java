package com.chatserver.resource.message;

import com.chatserver.api.response.BasicResponse;
import com.chatserver.core.StatusCode;
import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.swagger.annotations.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path(MessageResource.SERVICE_PATH)
@Api(value = MessageResource.SERVICE_PATH, description = "Operations about Message")
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {
    public final static String SERVICE_PATH = "/message";
    private final MessageService service;

    @Inject
    public MessageResource(MessageService service){
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    @Path("/{idCreator}/{idReceiver}")
    @Consumes(MediaType.APPLICATION_JSON)
    @ApiOperation( value = "Create a message", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.CREATED, message = "The message has been created"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error") })
    public BasicResponse create(
            @ApiParam(value = "creator to message", required = true)
                    @PathParam("idCreator") long idCreator,
            @ApiParam(value = "receiver to message", required = true)
                    @PathParam("idReceiver") long idReceiver,
            @ApiParam(value = "Message",  required = true)
                    @QueryParam("msg") String msg) {
        return service.create(idCreator, idReceiver, msg);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{idUser}")
    @ApiOperation( value = "Gets the message by idUser", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Message found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Message not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getById(@ApiParam(value = "Message ID needed to find the message", required = true)
                                    @PathParam("idUser") long idUser) {
        return service.getAllByIdUser(idUser);
    }

    @GET
    @Timed
    @UnitOfWork
    @Path("/{userOne}/{userTwo}")
    @ApiOperation( value = "Get messages between two users", response = BasicResponse.class)
    @ApiResponses(value = {
            @ApiResponse(code = StatusCode.OK, message = "Message found"),
            @ApiResponse(code = StatusCode.NOT_FOUND, message = "Message not found"),
            @ApiResponse(code = StatusCode.INTERNAL_SERVER_ERROR, message = "Internal server error")})
    public BasicResponse getAllByUserOneAndUserTwo(@ApiParam(value = "Message ID needed to find the message", required = true)
                                                       @PathParam("userOne") long userOne,
                                                   @ApiParam(value = "Message ID needed to find the message", required = true)
                                                        @PathParam("userTwo") long userTwo) {
        return service.getAllByUserOneAndUserTwo(userOne, userTwo);
    }
}
