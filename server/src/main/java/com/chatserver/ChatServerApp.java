package com.chatserver;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.chatserver.config.HbnBundle;
import com.chatserver.config.ChatServerConfig;
import com.chatserver.config.modules.HbnModule;
import com.chatserver.config.modules.MainModule;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import ru.vyarus.dropwizard.guice.GuiceBundle;
import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class ChatServerApp extends Application<ChatServerConfig> {
    public static void main(String[] args) throws Exception {
        new ChatServerApp().run(args);
    }

    @Override
    public String getName() {
        return "ChatServer";
    }

    @Override
    public void initialize(final Bootstrap<ChatServerConfig> bootstrap) {
        final HbnBundle hibernate = new HbnBundle();
        bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
        bootstrap.addBundle(hibernate);
        bootstrap.addBundle(GuiceBundle.builder()
                .enableAutoConfig(getClass().getPackage().getName())
                .modules(new HbnModule(hibernate), new MainModule())
                .build());
        bootstrap.addBundle(new MigrationsBundle<ChatServerConfig>() {
            @Override
            public DataSourceFactory getDataSourceFactory(ChatServerConfig configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(final ChatServerConfig configuration,
                    final Environment environment){
        this.setCORSconfiguration(environment);

        //Initializing apiListingResource
        environment.jersey().register(new ApiListingResource());
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        BeanConfig config = new BeanConfig();
        config.setTitle("Chat Server");
        config.setVersion("1.0.0");
        config.setBasePath("/api");
        config.setResourcePackage(getClass().getPackage().getName());
        config.setScan(true);
    }

    private void setCORSconfiguration(Environment environment) {
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.EXPOSED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,username,password");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM,
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,Location,username,password");
        filter.setInitParameter(CrossOriginFilter.ALLOW_CREDENTIALS_PARAM, "true");
    }
}
