package com.chatserver.dao.role;

import com.chatserver.model.Role;
import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class RoleRealDAO extends AbstractDAO<Role> implements RoleDAO{

    @Inject
    public RoleRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Role role) throws Exception {
        final Role created = persist(role);
        if(created == null) throw new Exception("the role was not created.");
        return created.getId();
    }

    @Override
    public Role getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public List<Role> getAll() throws Exception {
        return list(namedQuery("com.chatserver.model.Role.getAll"));
    }
}
