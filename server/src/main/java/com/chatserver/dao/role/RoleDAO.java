package com.chatserver.dao.role;

import com.chatserver.model.Role;
import java.util.List;

public interface RoleDAO {
    long create(Role role) throws Exception;
    Role getById(long id) throws Exception;
    List<Role> getAll() throws Exception;
}
