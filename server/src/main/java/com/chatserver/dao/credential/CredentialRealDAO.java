package com.chatserver.dao.credential;

import com.chatserver.model.Credential;
import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

public class CredentialRealDAO extends AbstractDAO<Credential> implements CredentialDAO {

    @Inject
    public CredentialRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Credential credential) throws Exception {
        final Credential created = persist(credential);
        if(created == null) throw new Exception("Credential was not created.");
        return created.getId();
    }

    @Override
    public Credential getById(long id) throws Exception {
        return get(id);
    }

    @Override
    public Credential login(String username, String password) throws Exception {
        return uniqueResult(namedQuery("com.chatserver.model.Credential.login").setParameter("username", username).setParameter("password", password));
    }

    @Override
    public Credential getByUsername(String username) throws Exception {
        return uniqueResult(namedQuery("com.chatserver.model.Credential.getByUsername").setParameter("username", username));
    }

    @Override
    public Credential delete(long id) throws Exception {
        Credential credentialDelete = get(id);
        if(credentialDelete.isState() == true) credentialDelete.setState(false);
        return credentialDelete;
    }

    @Override
    public Credential modify(Credential credential) throws Exception {
        Credential credentialModify = get(credential.getId());
        if(credential.getFirstName() != null) credentialModify.setFirstName(credential.getFirstName());
        if(credential.getLastName() != null) credentialModify.setLastName(credential.getLastName());

        return persist(credentialModify);
    }

    @Override
    public List<Credential> getCredentialActive() throws Exception {
        return list(namedQuery("com.chatserver.model.Credential.getCredentialActive"));
    }
}
