package com.chatserver.dao.credential;

import com.chatserver.model.Credential;
import java.util.List;

public interface CredentialDAO {
    long create(Credential credential) throws Exception;
    Credential getById(long id) throws Exception;
    Credential login (String username, String password) throws Exception;
    Credential getByUsername(String username) throws Exception;
    Credential delete(long id) throws Exception;
    Credential modify(Credential credential) throws Exception;
    List<Credential> getCredentialActive() throws Exception;
}
