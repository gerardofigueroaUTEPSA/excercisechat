package com.chatserver.dao.message;

import com.chatserver.model.Message;
import com.google.inject.Inject;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import java.util.List;

public class MessageRealDAO extends AbstractDAO<Message> implements MessageDAO {

    @Inject
    public MessageRealDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public long create(Message message) throws Exception {
        return persist(message).getId();
    }

    @Override
    public List<Message> getAllByIdUser(long idUser) throws Exception {
        return currentSession().createSQLQuery("select * from message where id_receiver = "+idUser).addEntity(Message.class).list();
    }

    @Override
    public List<Message> getAllByUserOneAndUserTwo(long userOne, long userTwo) throws Exception {
        return currentSession().createSQLQuery("select * from message where (id_creator = "+userOne+" and id_receiver = "+userTwo+") or (id_creator = "+userTwo+" and id_receiver = "+userOne+") ")
                .addEntity(Message.class).list();
    }

    @Override
    public void seenMessage(List<Message> messages) throws Exception {
        for (Message message: messages) {
            if(message.getDateSeen() == null){
                Message modify = get(message.getId());
                DateTime date = new DateTime();
                modify.setDateSeen(date);
                persist(modify);
            }
        }
    }
}
