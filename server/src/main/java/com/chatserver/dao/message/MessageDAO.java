package com.chatserver.dao.message;

import com.chatserver.model.Message;
import java.util.List;

public interface MessageDAO {
    long create(Message message) throws Exception;
    List<Message> getAllByIdUser(long idUser) throws Exception;
    List<Message> getAllByUserOneAndUserTwo(long userOne, long userTwo) throws Exception;
    void seenMessage(List<Message> messages) throws Exception;
}
