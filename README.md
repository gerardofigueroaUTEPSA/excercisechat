# ExcerciseChat Backend

- Install Maven.
- Install Intellij IDEA.
- Import Project with Intellij IDEA from the pom.xml.
- Active the option of "Import Maven Projects Automatically".
- be located in the console inside the "server" folder.
- open config.yml and modify the user, password and database name by our preference settings.
- server> mvn clean install.
- server> java -jar [path with target/chatserver-1.0-SNAPSHOT.jar] db migrate [path with config.yml].
- [src/main/resources/firsdata.sql] insert this query in the database.
- option menu [run>edit configurations> application].
	Main Class: com.chatserver.ChatServerApp.
	Program Arguments: server config.yml.
- Run Project.
- in this direction this swagger [http://localhost:9090/apidocs/index.html].

# ExcerciseChat Frontend

- Install NodeJs
- open console node
- go to the root directory of the ui 
- npm install
- npm run dev
- the ui will be raised in http:localhost:8080

# Used Technologies

- Backend with Dropwizard (Java) 
- Frontend with VueJs 2 
- Database PostgreSQL